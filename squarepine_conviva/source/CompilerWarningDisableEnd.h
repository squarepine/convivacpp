//==============================================================================
#if IS_MSVC
    #pragma warning (pop)
#elif IS_CLANG
    #pragma clang diagnostic pop
#elif IS_GCC
    #pragma GCC diagnostic pop
#else
    #error "Unknown compiler!"
#endif

//==============================================================================
#undef IS_MSVC
#undef IS_CLANG
#undef IS_GCC
