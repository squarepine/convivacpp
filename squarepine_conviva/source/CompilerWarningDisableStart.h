//==============================================================================
#undef IS_MSVC
#undef IS_CLANG
#undef IS_GCC

#define IS_MSVC     _MSC_VER && (_WIN32 || _WIN64 || _DURANGO)
#define IS_CLANG    __clang__
#define IS_GCC      __MINGW32__ || __MINGW64__ || __GNUC__ || __GNUG__

//==============================================================================
#if IS_MSVC
    #pragma warning (push, 1)
    #pragma warning (disable: 4150)
#elif IS_CLANG
    #pragma clang diagnostic push
    #pragma clang diagnostic ignored "-Wall"
#elif IS_GCC
    #pragma GCC diagnostic push
    #pragma GCC diagnostic ignored "-Wall"
#else
    #error "Unknown compiler!"
#endif
