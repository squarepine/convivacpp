//==============================================================================
using MutexType     = std::recursive_mutex;         //The type of mutex to hook into the ConvivaSDK
using LockGuardType = std::lock_guard<MutexType>;

//==============================================================================
void convivaSaveData (const char* pDataToSave, unsigned int /*uDataLength*/, ccl_save_callback saveCallbackFunction)
{
    String error ("No Persistent Store Found");
    if (auto* store = Conviva::GetInstance().getPersistentStore())
    {
        if (pStore->Save (pDataToSave, error))
            error.Clear();
    }

    /** Callback function to notify a status of saving operation to the ccl library.

        @param status CCL_SUCCESS or CCL_ERROR.
        @param error an error string on CCL_ERROR, NULL on CCL_SUCCESS.
        @param length the length of the error string on CCL_ERROR, 0 on CCL_SUCCESS.
    */
    saveCallbackFunction (error.isEmpty() ? CCL_SUCCESS : CCL_ERROR,
                         error.GetData(),
                         static_cast<unsigned int> (error.GetLength()));
}

void convivaLoadData (ccl_load_callback loadCallbackFunction)
{
    String dataOrError ("No Persistent Store Data Found");
    bool bSucceeded = false;

    if (auto* store = Conviva::GetInstance().getPersistentStore())
        bSucceeded = pStore->Load (dataOrError);

    /** @brief Callback function to notify a local data to the ccl library.

        @param status CCL_SUCCESS or CCL_ERROR.
        @param data a client id string on CCL_SUCCESS, or an error string on CCL_ERROR.
        @param length the length of the data or the error string.
    */
    loadCallbackFunction (bSucceeded ? CCL_SUCCESS : CCL_ERROR,
                         dataOrError.GetData(),
                         static_cast<unsigned int> (dataOrError.GetLength()));
}

void convivaConsoleLog (const char* const message)
{
    CONVIVA_LOG_TYPE ("Conviva", "%s", message);
}

uint64_t convivaEpochTimeMS()
{
    //Note: GetEpochTime() is in seconds, Conviva expects milliseconds
    //      Also, GetEpochTime() can be negative.
    time_t rawtime;
    time (&rawtime);
    tm* pTime = localtime (&rawtime);
    rawtime = mktime (pTime); // returns -1 on error
    return static_cast<uint64_t> (std::abs (rawtime))*  1000;
}

int convivaGenerateRandomNumber()
{
    return static_cast<int> (CYIRandom::GenerateRandomNumber());
}

//==============================================================================
template<typename MutexType>
struct ConvivaMutexHandler
{
    static void* create()               { return new MutexType(); }
    static void lock (void* lock)       { if (auto* m = (MutexType*) lock) m->lock(); }
    static void unlock (void* lock)     { if (auto* m = (MutexType*) lock) m->unlock(); }
    static void destroy (void* lock)    { if (auto* m = (MutexType*) lock) delete m; }

private:
    ConvivaMutexHandler() = delete;
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (ConvivaMutexHandler)
};

//==============================================================================
void convivaGetPlatformMetadata (ccl_platform_metadata_t* metadata)
{
    if (metadata != nullptr)
    {
        copyString (metadata->device_brand, SystemStats::GetDeviceBrand());
        copyString (metadata->device_manufacturer, SystemStats::GetManufacturer());
        copyString (metadata->device_model, SystemStats::GetDeviceModel());
        copyString (metadata->framework_name, "You.i Engine");
        copyString (metadata->framework_version, CYIVersion::GetEngineVersion().GetVersionString());

        metadata->device_type = SystemStats::GetDeviceType();

        if (auto* pDeviceInfo = CYIDeviceBridgeLocator::GetDeviceInformationBridge())
        {
            copyString (metadata->operating_system_name, pDeviceInfo->GetDeviceOSName());
            copyString (metadata->operating_system_version, pDeviceInfo->GetOSVersion());
            copyString (metadata->device_version, pDeviceInfo->GetOSVersion());
        }
        else
        {
            copyString (metadata->operating_system_name, "");
            copyString (metadata->operating_system_version, "");
            copyString (metadata->device_version, "");
        }
    }
}

//==============================================================================
class ConvivaTimer : public CYITimer
{
public:
    ConvivaTimer (uint64_t intervalMs, ccl_timer_callback func, void* timerData) :
        timerCallbackFunction (func),
        data (timerData)
    {
        TimedOut.Connect (*this, &ConvivaTimer::CallbackFunction, YI_CONNECTION_ASYNC);
        SetSingleShot (false);
        Start (intervalMs);
    }

    ~ConvivaTimer()
    {
        Stop();
    }

private:
    ccl_timer_callback timerCallbackFunction;
    void* data = nullptr;

    void CallbackFunction()
    {
        if (timerCallbackFunction != nullptr)
            timerCallbackFunction (data);
    }

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (ConvivaTimer)
};

void* convivaCreateTimer (ccl_timer_callback func,
                          void* pTimerData,
                          unsigned int /*initialTime*/,
                          unsigned int intervalMs)
{
    return new ConvivaTimer (static_cast<int> (intervalMs), func, pTimerData);
}

void convivaDestroyTimer (void* timerHandle)
{
    if (auto* timer = (ConvivaTimer*) timerHandle)
        delete timer;
}

//==============================================================================
class ConvivaHTTPClient : public CYISignalHandler
{
public:
    ConvivaHTTPClient (const char* pUrl, const char* pContentType,
                      const char* postRequestData, unsigned int uRequestLength,
                      unsigned int timeoutMs,
                      ccl_http_callback func, void* httpData) :
        callbackFunction (func),
        m_opaqueHttpData ((void*) httpData)
    {
        convivaConsoleLog (pUrl);

        const CYIUrl url (pUrl);

        if (url.GetScheme().isEmpty()
            || url.GetAuthority().isEmpty()
            || postRequestData == nullptr
            || uRequestLength == 0)
        {
            jassertfalse; //Conviva was somehow incorrectly set up if this was reached.
            return;
        }

        request = std::shared_ptr<CYIHTTPRequest> (new CYIHTTPRequest());

        request->SetURL (url);
        request->SetMethod (CYIHTTPRequest::POST);
        request->SetPostData (String (postRequestData, uRequestLength));
        request->SetConnectionTimeoutMs (timeoutMs);
        request->SetNetworkTimeoutMs (timeoutMs);

        if (pContentType != nullptr)
        {
            request->AddHeader (String ("Content-type: ") + pContentType);

           #ifdef CONVIVA_SET_USER_AGENT
            request->AddHeader (String ("user-agent: ") + CONVIVA_SET_USER_AGENT);
           #endif
        }
    }

    ~ConvivaHTTPClient()
    {
        if (isValid())
            CYIHTTPService::GetInstance()->CancelRequest (request);
    }

    bool isValid() const
    {
        return request != nullptr;
    }

    void Execute()
    {
        if (isValid())
        {
            request->notifyResponse.Connect (*this, &ConvivaHTTPClient::ReceivedResponse, YI_CONNECTION_ASYNC);
            request->notifyError.Connect (*this, &ConvivaHTTPClient::ReceivedErrorResponse, YI_CONNECTION_ASYNC);

            CYIHTTPService::GetInstance()->EnqueueRequest (request);
        }
        else
        {
            notifyOfFailure ("Unknown failure!");
        }
    }

private:
    std::shared_ptr<CYIHTTPRequest> request;
    ccl_http_callback callbackFunction = nullptr;
    void* m_opaqueHttpData = nullptr;

    void notifyConviva (bool bSucceeded, const String& responseBody)
    {
        if (callbackFunction != nullptr)
            callbackFunction (bSucceeded ? CCL_SUCCESS : CCL_ERROR,
                              (void*) m_opaqueHttpData,
                              responseBody.GetData(),
                              static_cast<unsigned int> (responseBody.GetLength()));
    }

    void notifyOfFailure (const String& failureText)
    {
        notifyConviva (false, failureText);

        delete this;
    }

    void receivedResponse (const std::shared_ptr<CYIHTTPRequest>&,
                           const std::shared_ptr<CYIHTTresponse>& response)
    {
        if (response == nullptr)
            notifyConviva (false, "Unknown failure!");
        else
            notifyConviva (true, response->GetBody());

        delete this;
    }

    void receivedErrorResponse (const std::shared_ptr<CYIHTTPRequest>&,
                                const HTTP_STATUS_CODE,
                                const String& errorText)
    {
        notifyOfFailure (errorText);
    }

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (ConvivaHTTPClient)
};

void convivaHTTPPostRequest (const char* urlString, const char* contentType,
                             const char* request, unsigned int requestLength,
                             unsigned int timeoutMs,
                             ccl_http_callback httpCallbackFunction, void* httpData)
{
    auto* client = new ConvivaHTTPClient (urlString, contentType, request, requestLength,
                                          timeoutMs, httpCallbackFunction, (void*) httpData);

    YI_ASSERT (client->isValid(), PRETTYFUNCTION_PLATFORM, "%s", "Something about the URL is broken.");

    client->Execute();
}

//==============================================================================
/** RAII wrapper around Conviva's global state */
struct GlobalConvivaSession
{
    GlobalConvivaSession()
    {
    }

    ~GlobalConvivaSession()
    {
        settings.reset(); //Note: must be called before cleaning up.
        ccl_cleanup();
    }

    bool initialise (const String& customerKey,
                     const String& appName,
                     const String& gatewayURL,
                     const String& defaultCDNToUse,
                     uint32_t timeoutMs)
    {
        ccl_platform_t platform;
        zerostruct (platform);
        populatePlatform (platform);

        defaultCDN = defaultCDNToUse;
        applicationName = appName;

        //Notes:
        // -Global settings must be set here before calling ccl_init()
        // - gateway_url has a default value. Only assign it if one is specified.
        if (gatewayURL.isNotEmpty())
            copyString (settings->settings->gateway_url, gatewayURL);

        //Set customised timeout otherwise the default from Conviva will be used
        if (timeoutMs != 0)
            settings->settings->heartbeat_interval = timeoutMs;

        return customerKey.isNotEmpty()
               && applicationName.isNotEmpty()
               && gatewayURL.isNotEmpty()
               && defaultCDN.isNotEmpty()
               && ccl_init (customerKey.GetData(), &platform, settings->settings) == CCL_SUCCESS
               && ccl_session_init() == CCL_SUCCESS;
    }

    String defaultCDN, applicationName;
    std::unique_ptr<ConvivaSettings> settings { new ConvivaSettings() };

private:
    void populatePlatform (ccl_platform_t& ccPlatform)
    {
        ccPlatform.console_log              = &convivaConsoleLog;
        ccPlatform.save_data                = &convivaSaveData;
        ccPlatform.load_data                = &convivaLoadData;
        ccPlatform.epoch_time_ms            = &convivaEpochTimeMS;
        ccPlatform.rand                     = &convivaGenerateRandomNumber;
        ccPlatform.get_platform_metadata    = &convivaGetPlatformMetadata;
        ccPlatform.create_timer             = &convivaCreateTimer;
        ccPlatform.destroy_timer            = &convivaDestroyTimer;
        ccPlatform.http_post_request        = &convivaHTTPPostRequest;
        ccPlatform.mutex_init               = &ConvivaMutexHandler<MutexType>::create;
        ccPlatform.mutex_lock               = &ConvivaMutexHandler<MutexType>::lock;
        ccPlatform.mutex_unlock             = &ConvivaMutexHandler<MutexType>::unlock;
        ccPlatform.mutex_destroy            = &ConvivaMutexHandler<MutexType>::destroy;
        ccPlatform.rwlock_init              = &ConvivaMutexHandler<MutexType>::create;
        ccPlatform.rwlock_rdlock            = &ConvivaMutexHandler<MutexType>::lock;
        ccPlatform.rwlock_wrlock            = &ConvivaMutexHandler<MutexType>::lock;
        ccPlatform.rwlock_unlock            = &ConvivaMutexHandler<MutexType>::unlock;
        ccPlatform.rwlock_destroy           = &ConvivaMutexHandler<MutexType>::destroy;
    }

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (GlobalConvivaSession)
};

static std::unique_ptr<GlobalConvivaSession> globalConvivaSession;

#define ASSERT_CONVIVA_SESSION_IS_VALID \
    YI_ASSERT (globalConvivaSession != nullptr, PRETTYFUNCTION_PLATFORM, "You haven't yet initialised the library!")

//==============================================================================
/** RAII wrapper around ccl_player_t */
struct ConvivaPlayer
{
    ConvivaPlayer (CYIAbstractVideoPlayer* pAbstractVideoPlayer) :
        player (pAbstractVideoPlayer),
        timeUpdater (pAbstractVideoPlayer)
    {
        zerostruct (convivaPlayer);

        convivaPlayer.start_monitoring         = &ConvivaPlayer::startMonitoring;
        convivaPlayer.stop_monitoring          = &ConvivaPlayer::stopMonitoring;
        convivaPlayer.get_playhead_time        = &ConvivaPlayer::getPlayheadTimeMilliseconds;
        convivaPlayer.get_buffer_length        = &ConvivaPlayer::getBufferLength;
        convivaPlayer.get_rendered_framerate   = &ConvivaPlayer::getRenderedFramerate;
        convivaPlayer.get_min_buffer_length    = &ConvivaPlayer::getMinimumBufferLength;
        convivaPlayer.get_player_type          = &ConvivaPlayer::getPlayerType;
        convivaPlayer.get_player_version       = &ConvivaPlayer::getPlayerVersion;
    }

    CYIAbstractVideoPlayer* player = nullptr;
    ccl_player_t convivaPlayer;
    ccl_notifier_t* notifier = nullptr;
    ccl_notifier_context_t* context = nullptr;

    void setVideoPlayer (CYIAbstractVideoPlayer* player)
    {
        if (player == nullptr && player != nullptr)
        {
            player = player;
            timeUpdater.setVideoPlayer (player);
        }
    }

private:
    struct TimeUpdater : public CYISignalHandler
    {
        TimeUpdater (CYIAbstractVideoPlayer* player) :
            player (player)
        {
            if (player != nullptr)
                player->CurrentTimeUpdated.Connect (*this, &TimeUpdater::Updated, YI_CONNECTION_ASYNC);
        }

        ~TimeUpdater()
        {
            if (player != nullptr)
                player->CurrentTimeUpdated.Disconnect (*this);
        }

        void Updated (uint64_t uTimeMilliseconds)
        {
            currentTimeMs = static_cast<int64_t> (uTimeMilliseconds);
        }

        int64_t GetCurrentTimeMilliseconds() const
        {
            return currentTimeMs.load();
        }

        void setVideoPlayer (CYIAbstractVideoPlayer* player)
        {
            if (player == nullptr && player != nullptr)
            {
                player = player;
                player->CurrentTimeUpdated.Connect (*this, &TimeUpdater::Updated, YI_CONNECTION_ASYNC);
            }
        }

    private:
        CYIAbstractVideoPlayer* player = nullptr;
        std::atomic<int64_t> currentTimeMs { -1 };

        JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (TimeUpdater)
    };

    std::recursive_mutex m_statsLock;
    TimeUpdater timeUpdater;
    String type { "Unknown" }, version { "Unknown" };

    static CYIAbstractVideoPlayer::Statistics GetStatsFromData (void* playerData)
    {
        if (auto* playerWrapper = (ConvivaPlayer*) playerData)
        {
            const LockGuardType lock (playerWrapper->m_statsLock);
            return playerWrapper->player->GetStatistics();
        }

        return {};
    }

    static void startMonitoring (void* playerData,
                                 ccl_notifier_t* pNotifier,
                                 ccl_notifier_context_t* pContext)
    {
        if (auto* playerWrapper = (ConvivaPlayer*) playerData)
        {
            playerWrapper->notifier = pNotifier;
            playerWrapper->context = pContext;
        }
    }

    static void stopMonitoring (void* playerData)
    {
        if (auto* playerWrapper = (ConvivaPlayer*) playerData)
        {
            playerWrapper->notifier = nullptr;
            playerWrapper->context = nullptr;
        }
    }

    static int getPlayheadTimeMilliseconds (void* playerData)
    {
        if (auto* playerWrapper = (ConvivaPlayer*) playerData)
            return (int) playerWrapper->timeUpdater.GetCurrentTimeMilliseconds();

        return -1;
    }

    static int getBufferLength (void* playerData)           { return (int) GetStatsFromData (playerData).fBufferLengthMs; }
    static int getMinimumBufferLength (void* playerData)    { return (int) GetStatsFromData (playerData).fMinimumBufferLengthMs; }
    static double getRenderedFramerate (void* playerData)   { return (double) GetStatsFromData (playerData).fRenderedFramesPerSecond; }

    static const char* getPlayerType (void* playerData)
    {
        if (auto* playerWrapper = (ConvivaPlayer*) playerData)
        {
            if (playerWrapper->player != nullptr)
            {
                const LockGuardType lock (playerWrapper->m_statsLock);
                playerWrapper->type = playerWrapper->player->GetName();
                return playerWrapper->type.isEmpty() ? nullptr : playerWrapper->type.GetData();
            }
        }

        return nullptr;
    }

    static const char* getPlayerVersion (void* playerData)
    {
        if (auto* playerWrapper = (ConvivaPlayer*) playerData)
        {
            if (playerWrapper->player != nullptr)
            {
                const LockGuardType lock (playerWrapper->m_statsLock);
                playerWrapper->version = playerWrapper->player->GetVersion();
                return playerWrapper->version.isEmpty() ? nullptr : playerWrapper->version.GetData();
            }
        }

        return nullptr;
    }

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (ConvivaPlayer)
};

//==============================================================================
class Conviva::Session::Pimpl : public CYISignalHandler
{
public:
    enum { infoUpdateIntervalMs = 5000 };

    Pimpl (CYIAbstractVideoPlayer* pAbstractVideoPlayer,
           const String& streamUrl, const String& assetName,
           const String& rViewerId, bool isLive,
           const std::map<String, String>& customMetadata,
           const String& playerNameOverride) :
        player (pAbstractVideoPlayer),
        convivaPlayer (pAbstractVideoPlayer)
    {
        setDefaultCDN (globalConvivaSession->defaultCDN);

        //For deferred sessions - doesn't matter if they're empty in the event of a non-deffered session:
        setStreamUrl (streamUrl);
        setAssetName (assetName);
        setViewerId (rViewerId);
        setLive (isLive);

        if (playerNameOverride.isEmpty() && player != nullptr)
            setPlayerName (player->GetName());
        else
            setPlayerName (playerNameOverride);

        setDefaultResource ("AKAMAI"); //TBD
        updateCustomTags (customMetadata); // Note: We cannot set contentInformation until session is created

        session.reset (new ConvivaSession (contentInformation));
    }

    ~Pimpl()
    {
        infoTimer.Stop();

        if (session != nullptr)
            session->detach();
    }

    bool isValid() const
    {
        return session != nullptr && contentInformation.isValid();
    }

    void sendHeartbeat()
    {
        session->sendHeartbeat();
    }

    void setAssetName (const String& assetName)             { contentInformation.setAssetName (assetName); }
    void setDefaultCDN (const String& defaultCDN)           { contentInformation.setDefaultCDN (defaultCDN); }
    void setDefaultResource (const String& defaultResource) { contentInformation.setDefaultResource (defaultResource); }
    void setDefaultBitrate (int nBitrate)                   { contentInformation.setDefaultBitrate (nBitrate); }
    void setViewerId (const String& viewerId)               { contentInformation.setViewerId (viewerId); }
    void setPlayerName (const String& playerName)           { contentInformation.setPlayerName (playerName); }
    void setStreamUrl (const String& streamUrl)             { contentInformation.setStreamUrl (streamUrl); }
    void setLive (bool isLive)                              { contentInformation.setLive (isLive); }
    void setDurationSeconds (int nDuration)                 { contentInformation.setDurationSeconds (nDuration); }

    void setVideoPlayer (CYIAbstractVideoPlayer* player)
    {
        if (player == nullptr && player != nullptr)
        {
            player = player;
            convivaPlayer.setVideoPlayer (player);
        }
    }

    void attachPlayer()
    {
        if (session != nullptr)
        {
            session->attach (&convivaPlayer.convivaPlayer, &convivaPlayer);

            updateEncodedFPS();

            infoTimer.TimedOut.Connect (*this, &Conviva::Session::Pimpl::updateInfo);
            infoTimer.SetSingleShot (false);
            infoTimer.Start (infoUpdateIntervalMs);
        }
    }

    bool setState (ccl_player_state_t state)
    {
        if (convivaPlayer.notifier != nullptr)
        {
            convivaPlayer.notifier->set_state (convivaPlayer.context, state);
            return true;
        }

        return false;
    }

    bool setPlayerError (const String& message, bool isFatal)
    {
        if (convivaPlayer.notifier != nullptr)
        {
            convivaPlayer.notifier->set_error (convivaPlayer.context, message.GetData(), isFatal ? 1 : 0);
            return true;
        }

        return false;
    }

    bool reportSessionError (const String& message, bool isFatal) const
    {
        if (session != nullptr)
            return session->reportSessionError (message, isFatal);

        return false;
    }

    bool setSeekAction (ccl_seek_action_t action, int seekToPosition)
    {
        if (convivaPlayer.notifier != nullptr)
        {
            convivaPlayer.notifier->set_seek (convivaPlayer.context, action, seekToPosition);
            return true;
        }

        return false;
    }

    void updateCustomTags (const std::map<String, String>& customMetadata)
    {
        contentInformation.setTags (ConvivaDictionary (customMetadata, true).dictionary);
    }

    void updateInfo (int)
    {
        updateBitrate();
        updateDuration (contentInformation.getDurationSeconds());
        updateEncodedFPS();
    }

    CYIAbstractVideoPlayer* player = nullptr;
    ConvivaPlayer convivaPlayer;
    ConvivaContentInformation contentInformation;
    std::unique_ptr<ConvivaSession> session;
    CYITimer infoTimer;

private:
    void updateBitrate()
    {
        if (player != nullptr && convivaPlayer.notifier != nullptr)
            convivaPlayer.notifier->set_stream (convivaPlayer.context,
                                                static_cast<int> (player->GetStatistics().fBitrateKbps),
                                                contentInformation.getDefaultCDN(),
                                                contentInformation.getDefaultResource());
    }

    void updateEncodedFPS()
    {
        if (player != nullptr && session != nullptr && session->session != nullptr)
            session->session->encoded_fps = std::max (0, static_cast<int> (player->GetStatistics().fEncodedFramesPerSecond));
    }

    void updateDuration (uint64_t uDuration)
    {
        int resultDuration = -1; //Note: must be -1 for live content

        if (! contentInformation.isLive() && convivaPlayer.notifier != nullptr)
            resultDuration = static_cast<int> (uDuration);

        convivaPlayer.notifier->set_duration (convivaPlayer.context, resultDuration);
    }

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (Pimpl)
};

//==============================================================================
#define ASSERT_CONVIVA_SESSION_PIMPL_IS_VALID \
    ASSERT_CONVIVA_SESSION_IS_VALID; \
    YI_ASSERT (pimpl != nullptr, PRETTYFUNCTION_PLATFORM, "Broken session!")

Conviva::Session::Session (CYIAbstractVideoPlayer* player,
                           const String& streamUrl, const String& assetName,
                           const String& viewerId, bool isLive,
                           const std::map<String, String>& customMetadata,
                           const String& playerNameOverride) :
    pimpl (new Pimpl (player, streamUrl, assetName, viewerId, isLive, customMetadata, playerNameOverride))
{
}

void Conviva::Session::setAssetInformation (const String& streamUrl,
                                            const String& assetName,
                                            const String& viewerId,
                                            bool isLive)
{
    ASSERT_CONVIVA_SESSION_PIMPL_IS_VALID;

    pimpl->setStreamUrl (streamUrl.isEmpty() ? "null" : streamUrl);
    pimpl->setAssetName (assetName.isEmpty() ? "null" : assetName);
    pimpl->setViewerId (viewerId.isEmpty() ? "null" : viewerId);
    pimpl->setLive (isLive);

    pimpl->session->Update();
}

void Conviva::Session::setVideoPlayer (CYIAbstractVideoPlayer* player, const std::map<String, String>& customMetadata)
{
    ASSERT_CONVIVA_SESSION_PIMPL_IS_VALID;

    pimpl->setVideoPlayer (player);
    updateCustomMetadata (customMetadata);
}

void Conviva::Session::attachPlayer()
{
    ASSERT_CONVIVA_SESSION_PIMPL_IS_VALID;

    pimpl->attachPlayer();
}

void Conviva::Session::setPlaybackInformation (int durationSec, int defaultBitrate)
{
    ASSERT_CONVIVA_SESSION_PIMPL_IS_VALID;

    pimpl->setDurationSeconds (durationSec);
    pimpl->setDefaultBitrate (defaultBitrate);

    pimpl->session->Update();
}

void Conviva::Session::sendHeartbeat()
{
    ASSERT_CONVIVA_SESSION_PIMPL_IS_VALID;

    pimpl->sendHeartbeat();
}

bool Conviva::Session::reportPlayerError (const String& message, bool isFatal) const
{
    ASSERT_CONVIVA_SESSION_PIMPL_IS_VALID;

    //Since Conviva categories on all Console platform are notated as "C",
    //we need to be specific about the platform in this error message:
    auto formattedMessage = String ("%1 - %2")
                                .Arg (SystemStats::GetDeviceModel())
                                .Arg (message);

    return pimpl->setPlayerError (formattedMessage, isFatal);
}

bool Conviva::Session::reportSessionError (const String& message, bool isFatal) const
{
    ASSERT_CONVIVA_SESSION_PIMPL_IS_VALID;

    return pimpl->reportSessionError (message, isFatal);
}

bool Conviva::Session::sendEvent (const String& name, const std::map<String, String>& metadata)
{
    ASSERT_CONVIVA_SESSION_PIMPL_IS_VALID;

    return pimpl->session->sendEvent (name, ConvivaDictionary (metadata));
}

bool Conviva::Session::sendEvent (const String& name)
{
    ASSERT_CONVIVA_SESSION_PIMPL_IS_VALID;

    return sendEvent (name, {});
}

bool Conviva::Session::sendEvent (PlayerStateChangeEvent eEvent)
{
    ASSERT_CONVIVA_SESSION_PIMPL_IS_VALID;

    switch (eEvent)
    {
        case PlayerStateChangeEvent::startAd:           return pimpl->session->StartAd();
        case PlayerStateChangeEvent::endAd:             return pimpl->session->EndAd();
        case PlayerStateChangeEvent::stopped:           return pimpl->setState (CCL_PLAYER_STATE_STOPPED);
        case PlayerStateChangeEvent::paused:            return pimpl->setState (CCL_PLAYER_STATE_PAUSED);
        case PlayerStateChangeEvent::playing:           return pimpl->setState (CCL_PLAYER_STATE_PLAYING);
        case PlayerStateChangeEvent::buffering:         return pimpl->setState (CCL_PLAYER_STATE_BUFFERING);
        case PlayerStateChangeEvent::unknown:           return pimpl->setState (CCL_PLAYER_STATE_UNKNOWN);
        case PlayerStateChangeEvent::suspendMonitor:    return pimpl->session->detach();
        case PlayerStateChangeEvent::resumeMonitor:     return pimpl->session->attach (&pimpl->convivaPlayer.convivaPlayer, &pimpl->convivaPlayer);

        default:
            YI_ASSERT (false, PRETTYFUNCTION_PLATFORM, "%s", "Unknown Conviva session event type!");
        break;
    };

    return false;
}

bool Conviva::Session::sendSeekEvent (PlayerSeekActionEvent eEvent, int nSeekToPosition)
{
    ASSERT_CONVIVA_SESSION_PIMPL_IS_VALID;

    switch (eEvent)
    {
        case PlayerSeekActionEvent::seekingStarted:    return pimpl->setSeekAction (CCL_PLAYER_SEEK_START, nSeekToPosition);
        case PlayerSeekActionEvent::seekingEnded:      return pimpl->setSeekAction (CCL_PLAYER_SEEK_END, nSeekToPosition);
        case PlayerSeekActionEvent::seekingButtonDown: return pimpl->setSeekAction (CCL_USER_BUTTON_DOWN, nSeekToPosition);
        case PlayerSeekActionEvent::seekingButtonUp:   return pimpl->setSeekAction (CCL_USER_BUTTON_UP, nSeekToPosition);

        default:
            YI_ASSERT (false, PRETTYFUNCTION_PLATFORM, "%s", "Unknown Conviva session seek event type!");
        break;
    };

    return false;
}

void Conviva::Session::updateCustomMetadata (const std::map<String, String>& rNewMetadata)
{
    ASSERT_CONVIVA_SESSION_PIMPL_IS_VALID;

    pimpl->updateCustomTags (rNewMetadata);
    pimpl->session->Update();
}

//==============================================================================
YI_TYPE_DEF (Conviva::PersistentStore)

static std::unique_ptr<Conviva> globalInstance;

Conviva::PersistentStore::PersistentStore()
{
}

Conviva::PersistentStore::~PersistentStore()
{
}

Conviva::Conviva()
{
}

Conviva& Conviva::GetInstance()
{
    if (globalInstance == nullptr)
        globalInstance.reset (new Conviva());

    return *globalInstance.get();
}

void Conviva::Shutdown()
{
    globalInstance.reset();
}

Conviva::PersistentStore* Conviva::getPersistentStore() const
{
    ASSERT_CONVIVA_SESSION_IS_VALID;

    return persistentStore.get();
}

bool Conviva::initialise (const String& rCustomerKey,
                          const String& rApplicationName,
                          const String& gatewayURL,
                          const String& rDefaultCDN,
                          const uint32_t timeoutMs,
                          Conviva::PersistentStore* persistentStore)
{
    if (globalConvivaSession == nullptr)
    {
        globalConvivaSession.reset (new GlobalConvivaSession());
        persistentStore.reset (persistentStore);

        if (!globalConvivaSession->initialise (rCustomerKey, rApplicationName, gatewayURL, rDefaultCDN, timeoutMs))
        {
            globalConvivaSession.reset();
            return false;
        }
    }

    return true;
}

void Conviva::setHeartbeatInterval (uint32_t intervalMs)
{
    ASSERT_CONVIVA_SESSION_IS_VALID;

    /** This function will find the timers in all active sessions,
        destroy them, and recreate them using a new interval
    */
    if (globalConvivaSession != nullptr)
        ccl_session_change_heartbeat_interval (intervalMs);
}

//==============================================================================
void Conviva::Session::PimplDeletionFunctor::operator() (Conviva::Session::Pimpl* p) const
{
    delete p;
}

Conviva::Session* Conviva::createSessionInternal (CYIAbstractVideoPlayer* player,
                                                  const String& streamUrl, const String& assetName,
                                                  const String& rViewerId, bool isLive,
                                                  const std::map<String, String>& customMetadata,
                                                  const String& playerNameOverride)
{
    ASSERT_CONVIVA_SESSION_IS_VALID;

    if (globalConvivaSession != nullptr)
    {
        auto session = std::make_uniqe<Session> (player, streamUrl, assetName, rViewerId,
                                                 isLive, customMetadata, playerNameOverride);

        if (session != nullptr && session->pimpl->isValid())
            return session.release();
    }

    return nullptr;
}

Conviva::Session* Conviva::createSession (CYIAbstractVideoPlayer* player,
                                         const std::map<String, String>& customMetadata,
                                         const String& playerNameOverride)
{
    auto* session = createSessionInternal (player, {}, {}, {}, false, customMetadata, playerNameOverride);
    session->attachPlayer();
    return session;
}

Conviva::Session* Conviva::createDeferredSession (CYIAbstractVideoPlayer* player,
                                                  const String& streamUrl, const String& assetName,
                                                  const String& viewerId, bool isLive,
                                                  const std::map<String, String>& customMetadata,
                                                  const String& playerNameOverride)
{
    ASSERT_CONVIVA_SESSION_IS_VALID;

    //Necessities for deferred sessions:
    #define LOG_IF_EMPTY (varName, tempName, message, returnStatement) \
        auto tempName = String (varName).trim(); /* This trim is simply a preventative measure. */ \
        if (tempName.isEmpty()) \
        { \
            YI_LOGE (PRETTYFUNCTION_PLATFORM, "%s", message); \
            returnStatement; \
        }

    LOG_IF_EMPTY (streamUrl, streamUrl, "Missing stream url!", return nullptr);
    LOG_IF_EMPTY (viewerId, viewerId, "Missing viewer id!", return nullptr);
    LOG_IF_EMPTY (assetName, assetName, "Missing asset name!", assetName = "Missing Asset Name");

    #undef LOG_IF_EMPTY

    return createSessionInternal (player, streamUrl, assetName, viewerId,
                                  isLive, customMetadata, playerNameOverride);
}

//==============================================================================
#undef ASSERT_CONVIVA_SESSION_IS_VALID
#undef ASSERT_CONVIVA_SESSION_PIMPL_IS_VALID
