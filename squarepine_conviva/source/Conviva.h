/** The global Conviva singleton.

    @warning All function calls must be done on the main thread!
*/
class Conviva
{
public:
    //==============================================================================
    /** Get the singleton instance. */
    static Conviva& getInstance();

    /** Does an explicit early shutdown of the global Conviva system.

        It's not necessary to call this, as the static instance will be automatically cleaned up.
        This is more of a convenience function for the resolving few edge-case circumstances.
    */
    static void shutdown();

    //==============================================================================
    /** */
    class PersistentStore
    {
    public:
        PersistentStore();
        virtual ~PersistentStore();

        virtual bool Save (const String& data, String& error) = 0;
        virtual bool Load (String& data) = 0;

    private:
        JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (PersistentStore)
    };

    /**

        @warning This must only be done once!

        @param[in] customerKey      Must not be empty!
        @param[in] applicationName  Must not be empty!
        @param[in] gatewayURL       Must not be empty!
        @param[in] defaultCDN       Must not be empty!
        @param[in] timeoutMs        Default heartbeat interval to start with, in milliseconds.
        @param[in] persistentStore  Must not be null for production purposes.
                                    Will be owned by the Conviva singleton.

        @see setHeartbeatInterval
    */
    bool initialise (const String& customerKey,
                     const String& applicationName,
                     const String& gatewayURL,
                     const String& defaultCDN,
                     uint32_t timeoutMs = 0,
                     PersistentStore* persistentStore = nullptr);

    /** */
    PersistentStore* getPersistentStore() const;

    //==============================================================================
    /** Sets the global Conviva heartbeat interval.

        The default heartbeat interval is 20 seconds.

        Note that this will subsequently change heartbeat intervals for active sessions.
    */
    void setHeartbeatInterval (uint32_t intervalMs);

    //==============================================================================
    /** A Conviva session, that performs metric calls.

        To make use of a Session, create one via the global Conviva instance:
        @see Conviva::createSession

        @warning All function calls must be done on the main thread!
    */
    class Session
    {
    public:
        void setAssetInformation (const String& streamUrl,
                                  const String& assetName,
                                  const String& viewerId,
                                  bool isLive);

        void setPlaybackInformation (int duration, int defaultBitrate);

        /** Manually forces an update server event. */
        void sendHeartbeat();

        /** Sends a custom message event. */
        bool sendEvent (const String& name);

        /** Sends a custom message event, with a block of metadata. */
        bool sendEvent (const String& name, const std::map<String, String>& metadata);

        /** Set the player (if one does not already exist). */
        void setVideoPlayer (CYIAbstractVideoPlayer* player, const std::map<String, String>& customMetadata = {});

        /** After creating a deferred session, this must be called to attach the player immediately before playback,
            after all DRM and entitlement requests have succeeded.
        */
        void attachPlayer();

        enum class PlayerStateChangeEvent
        {
            startAd,
            endAd,
            stopped,
            paused,
            playing,
            buffering,
            suspendMonitor,
            resumeMonitor,
            unknown
        };

        enum class PlayerSeekActionEvent
        {
            seekingStarted,
            seekingEnded,
            seekingButtonDown,
            seekingButtonUp
        };

        /** Sends a standardised Conviva event. */
        bool sendEvent (PlayerStateChangeEvent event);

        /** Sends a player seek event. */
        bool sendSeekEvent (PlayerSeekActionEvent event, int seekToPosition);

        /** Reports an player error. */
        bool reportPlayerError (const String& message, bool isFatal) const;

        /** Reports a session error. */
        bool reportSessionError (const String& message, bool isFatal) const;

        /** @note According to Conviva, value modification for the same key will not take effect,
                  thus it can only be set once.
        */
        void updateCustomMetadata (const std::map<String, String>& customMetadata);

    private:
        Session (CYIAbstractVideoPlayer* player,
                 const String& streamUrl, const String& assetName,
                 const String& viewerId, bool isLive,
                 const std::map<String, String>& customMetadata,
                 const String& playerNameOverride);

        class Pimpl;
        struct PimplDeletionFunctor
        {
            constexpr PimplDeletionFunctor() noexcept = default;
            void operator() (Pimpl* p) const;
        };

        std::unique_ptr<Pimpl, PimplDeletionFunctor> pimpl;

        friend class Conviva;

        Session() = delete;
        JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (Session)
    };

    /** create a player and asset bound session.

        Sessions are contextualised around a single player, and its single video asset.
        If the asset changes, the session must be recreated.  A session need not have a player.
        If there is no player, then it is expected that the initialization of the player is
        deferred until a player is known and set through setVideoPlayer.

        Note that creating a session, and calling any functions, must be done on the main thread.
        Not doing so risks catastrophic failure.

        @param[in] player              Careful that the player does not go out of scope before the session does!
        @param[in] customMetadata      A list of metadata. This metadata is provided by the client and
                                        Conviva in spreadsheet form. Please discuss obtaining their spreadsheet
                                        to be able to fill out the information appropriately.
        @param[in] playerNameOverride  Use this to override with a custom player name

    */
    Session* createSession (CYIAbstractVideoPlayer* player,
                            const std::map<String, String>& customMetadata = {},
                            const String& playerNameOverride = {});

    /** create a player and asset bound deferred session.

        For more information about deferred sessions:
        https://community.conviva.com/site/global/develop/c_sdk_new/taskref/deferred/index.gsp  

        Sessions are contextualised around a single player, and its single video asset.
        If the asset changes, the session must be recreated.  A session need not have a player.
        If there is no player, then it is expected that the initialization of the player is
        deferred until a player is known and set through setVideoPlayer.

        Note that creating a session, and calling any functions, must be done on the main thread.
        Not doing so risks catastrophic failure.

        @param[in] player              Careful that the player does not go out of scope before the session does!
        @param[in] customMetadata      A list of metadata. This metadata is provided by the client and
                                        Conviva in spreadsheet form. Please discuss obtaining their spreadsheet
                                        to be able to fill out the information appropriately.
        @param[in] playerNameOverride  Use this to override with a custom player name
    */
    Session* createDeferredSession (CYIAbstractVideoPlayer* player,
                                    const String& streamUrl, const String& assetName,
                                    const String& rViewerId, bool isLive,
                                    const std::map<String, String>& customMetadata = {},
                                    const String& playerNameOverride = {});

private:
    //==============================================================================
    /** @internal */
    Conviva();

    /** @internal */
    Session* createSessionInternal (CYIAbstractVideoPlayer* player,
                                    const String& streamUrl, const String& assetName,
                                    const String& rViewerId, bool isLive,
                                    const std::map<String, String>& customMetadata,
                                    const String& playerNameOverride);

    std::unique_ptr<PersistentStore> persistentStore;

    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (Conviva)
};
