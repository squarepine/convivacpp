//==============================================================================
/** RAII wrapper around ccl_settings_t */
struct ConvivaSettings
{
    ConvivaSettings()
    {
       #if _DEBUG || JUCE_DEBUG
        settings->log_level = CCL_LOG_LEVEL_DEBUG;
       #elif defined (CONVIVA_PRODUCTION_ERROR_LEVEL)
        settings->log_level = CONVIVA_PRODUCTION_ERROR_LEVEL;
       #else
        settings->log_level = CCL_LOG_LEVEL_WARN;
       #endif
    }

    ~ConvivaSettings()
    {
        ccl_settings_destroy (settings);
    }

    ccl_settings_t* settings = ccl_settings_create();

private:
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (ConvivaSettings)
};

//==============================================================================
/** RAII wrapper around ccl_content_info_t */
class ConvivaContentInformation
{
public:
    ConvivaContentInformation()
    {
        contentInformation->asset_name = nullptr;
        contentInformation->default_cdn = nullptr;
        contentInformation->default_resource = nullptr;
        contentInformation->default_bitrate = -1;
        contentInformation->viewer_id = nullptr;
        contentInformation->player_name = nullptr;
        contentInformation->stream_url = nullptr;
        contentInformation->is_live = false;
        contentInformation->duration = -1;
        contentInformation->tags = nullptr;
    }

    ~ConvivaContentInformation()
    {
        std::free (contentInformation->asset_name);
        std::free (contentInformation->default_cdn);
        std::free (contentInformation->default_resource);
        std::free (contentInformation->viewer_id);
        std::free (contentInformation->player_name);
        std::free (contentInformation->stream_url);
        clearTags();
        ccl_content_info_destroy (contentInformation);
    }

    void setAssetName (const String& assetName)
    {
        std::free (contentInformation->asset_name);
        contentInformation->asset_name = nullptr;
        copyString (contentInformation->asset_name, assetName);
    }

    void setDefaultCDN (const String& defaultCDN)
    {
        std::free (contentInformation->default_cdn);
        contentInformation->default_cdn = nullptr;
        copyString (contentInformation->default_cdn, defaultCDN);
    }

    void setDefaultResource (const String& defaultResource)
    {
        std::free (contentInformation->default_resource);
        contentInformation->default_resource = nullptr;
        copyString (contentInformation->default_resource, defaultResource);
    }

    void setViewerId (const String& viewerId)
    {
        std::free (contentInformation->viewer_id);
        contentInformation->viewer_id = nullptr;
        copyString (contentInformation->viewer_id, viewerId);
    }

    void setPlayerName (const String& playerName)
    {
        std::free (contentInformation->player_name);
        contentInformation->player_name = nullptr;
        copyString (contentInformation->player_name, playerName);
    }

    void setStreamUrl (const String& streamUrl)
    {
        std::free (contentInformation->stream_url);
        contentInformation->stream_url = nullptr;
        copyString (contentInformation->stream_url, streamUrl);
    }

    void setDefaultBitrate (int nBitrate)   { contentInformation->default_bitrate = nBitrate; }
    void setLive (bool isLive)              { contentInformation->is_live = isLive; }
    void setDurationSeconds (int nDuration) { contentInformation->duration = nDuration; }

    const char* getDefaultResource() const  { return contentInformation->default_resource; }
    int getDefaultBitrate() const           { return contentInformation->default_bitrate; }
    const char* getAssetName() const        { return contentInformation->asset_name; }
    const char* getDefaultCDN() const       { return contentInformation->default_cdn; }
    const char* getViewerId() const         { return contentInformation->viewer_id; }
    const char* getPlayerName() const       { return contentInformation->player_name; }
    const char* GetStreamUrl() const        { return contentInformation->stream_url; }
    bool isLive() const                     { return contentInformation->is_live; }
    int getDurationSeconds() const          { return contentInformation->duration; }
    const ccl_dictionary_t* getTags() const { return contentInformation->tags; }
    bool isValid() const                    { return contentInformation != nullptr; }

    void setTags (ccl_dictionary_t* tags)
    {
        clearTags();
        contentInformation->tags = tags;
    }

private:
    friend class ConvivaSession;

    void clearTags()
    {
        if (contentInformation->tags != nullptr)
            ccl_dictionary_destroy (contentInformation->tags);
    }

    ccl_content_info_t* contentInformation = ccl_content_info_create();

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (ConvivaContentInformation)
};

//==============================================================================
/** RAII wrapper around ccl_queue_t */
struct ConvivaContentQueue
{
    ConvivaContentQueue() :
        queue (ccl_queue_create())
    {
    }

    ConvivaContentQueue (int maxSize) :
        queue (ccl_queue_create_size (maxSize))
    {
    }

    ~ConvivaContentQueue()
    {
        ccl_queue_destroy (queue);
    }

    int getSize() const         { return ccl_queue_size (queue); }
    void empty()                { ccl_queue_empty (queue); }
    void enqueue (void* pData)  { ccl_enqueue (queue, pData); }
    void* dequeue() const       { return ccl_dequeue (queue); }

    //==============================================================================
    static bool lock()      { return ccl_queue_init() == CCL_SUCCESS; }
    static void unlock()    { ccl_queue_done(); }

    struct AutoLock
    {
        AutoLock()  { ConvivaContentQueue::lock(); }
        ~AutoLock() { ConvivaContentQueue::unlock(); }

    private:
        JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (AutoLock)
    };

    ccl_queue_t* queue = nullptr;

private:
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (ConvivaContentQueue)
};

//==============================================================================
/** RAII wrapper around ccl_dictionary_t */
struct ConvivaDictionary
{
    ConvivaDictionary (bool manageLifetimeExternally = false) :
        dictionary (ccl_dictionary_create()),
        destroyAutomatically (! manageLifetimeExternally)
    {
    }

    explicit ConvivaDictionary (const std::map<String, String>& metadata,
                                bool manageLifetimeExternally = false)
        dictionary (ccl_dictionary_create())
        destroyAutomatically (! manageLifetimeExternally)
    {
        for (const auto& it : metadata)
            set (it.first, it.second);
    }

    ~ConvivaDictionary()
    {
        if (destroyAutomatically)
            ccl_dictionary_destroy (dictionary);
    }

    void set (const String& key, const String& value)
    {
        //Note: Conviva expects empty strings to appear as "null"
        auto correctedValue = value.trim();
        correctedValue = correctedValue.isEmpty() ? "null" : value;

        ccl_dictionary_put (dictionary, key.GetData(), correctedValue.GetData());
    }

    String get (const String& key) const
    {
        return ccl_dictionary_get (dictionary, key.GetData());
    }

    ccl_dictionary_t* dictionary = nullptr;

private:
    const bool destroyAutomatically;

    ConvivaDictionary() = delete;
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (ConvivaDictionary)
};

//==============================================================================
/** RAII wrapper around ccl_notifier_context_t. */
struct ConvivaNotifierContext
{
    ConvivaNotifierContext (ccl_session_t* pSession) :
        notifier (ccl_monitor_create (pSession))
    {
    }

    ~ConvivaNotifierContext()   { ccl_monitor_destroy (notifier); }
    void suspend()              { ccl_monitor_suspend (notifier); }
    void Resume()               { ccl_monitor_resume (notifier); }
    void StartAd()              { ccl_monitor_start_ad (notifier); }
    void EndAd()                { ccl_monitor_end_ad (notifier); }

    ccl_notifier_context_t* notifier = nullptr;

private:
    ConvivaNotifierContext() = delete;
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (ConvivaNotifierContext)
};

//==============================================================================
/** RAII wrapper around ccl_session_t. */
struct ConvivaSession
{
    /**
        @param contentInfo This object's lifetime will not be managed by the session!
    */
    ConvivaSession (ccl_content_info_t* pContentInfo) :
        session (ccl_session_create (pContentInfo)),
        contentInformation (pContentInfo)
    {
    }

    /**
        @param contentInfo This object's lifetime will not be managed by the session!
    */
    ConvivaSession (const ConvivaContentInformation& other) :
        session (ccl_session_create (other.contentInformation)),
        contentInformation (other.contentInformation)
    {
    }

    ~ConvivaSession()
    {
        detach();
        ccl_session_destroy (session);
    }

    bool attach (const ccl_player_t* player, void* playerData) const    { return ccl_session_player_attach (session, player, playerData) == CCL_SUCCESS; }
    bool detach() const                                                 { return ccl_session_player_detach (session) == CCL_SUCCESS; }
    bool startAd() const                                                { return ccl_session_ad_start (session) == CCL_SUCCESS; }
    bool endAd() const                                                  { return ccl_session_ad_end (session) == CCL_SUCCESS; }

    bool reportSessionError (const String& message, bool isFatal) const
    {
        return ccl_session_report_error (session, message.GetData(), isFatal ? 1 : 0) == CCL_SUCCESS;
    }

    bool sendEvent (const String& name, ccl_dictionary_t* attributes) const
    {
        return ccl_send_event (name.GetData(), attributes) == CCL_SUCCESS;
    }

    bool sendEvent (const String& name, const ConvivaDictionary& dictionary) const
    {
        return sendEvent (name, dictionary.dictionary);
    }

    void sendHeartbeat()
    {
        ccl_session_send_heartbeat (session);
    }

    void update()
    {
        ccl_session_update_content_info (session, contentInformation);
    }

    ccl_session_t* session = nullptr;

private:
    ccl_content_info_t* contentInformation = nullptr;

    ConvivaSession() = delete;
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (ConvivaSession)
};
