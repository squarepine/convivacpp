namespace SystemStats
{
    /** @warning All strings are provided by Conviva. */
    String getDeviceBrand()
    {
       #if YI_XBOX_360 || YI_XBOX_ONE || YI_UWP
        return "Xbox";
       #elif YI_PS3 || YI_PS4
        return "PlayStation";
       #elif YI_ANDROID
        return "Android";
       #elif YI_IOS
        return "iOS";
       #elif YI_TVOS
        return "TVOS";
       #elif YI_TIZEN_NACL
        return "Samsung";
       #elif YI_ROKU
        return "Roku";
       #elif YI_LINUX
        return "Linux";
       #elif YI_OSX
        return "MacOSX";
       #elif YI_WINDOWS || YI_WIN32 || YI_WIN64
        return "Windows";
       #elif YI_DEVICE_HANDSET
        return "Mobile";
       #elif YI_DEVICE_TABLET
        return "Tablet";
       #elif YI_DEVICE_CONSOLE || YI_CONSOLE
        return "Console";
       #else
        return "Unknown";
       #endif
    }

    String getDeviceModel()
    {
        return CYIDeviceBridgeLocator::GetDeviceInformationBridge()->GetDeviceModel();
    }

    /** @warning All strings are provided by Conviva. */
    String getManufacturer()
    {
       #if YI_WINDOWS || YI_WIN32 || YI_WIN64 || YI_XBOX_360 || YI_XBOX_ONE || YI_UWP
        return "Microsoft";
       #elif YI_PS3 || YI_PS4
        return "Sony";
       #elif YI_ANDROID
        return "Google";
       #elif YI_TIZEN_NACL
        return "Samsung";
       #elif YI_IOS || YI_TVOS || YI_OSX
        return "Apple Inc.";
       #elif YI_ROKU
        return "Roku, Inc.";
       #elif YI_LINUX
        return "null";
       #else
        return "Unknown";
       #endif
    }

    ccl_device_type_t getDeviceType()
    {
       #if YI_XBOX_360 || YI_XBOX_ONE || YI_PS3 || YI_PS4 || YI_UWP || YI_DEVICE_CONSOLE || YI_CONSOLE
        return CCL_DEVICE_TYPE_CONSOLE;
       #elif YI_ANDROID || YI_IOS || YI_DEVICE_HANDSET
        return CCL_DEVICE_TYPE_MOBILE;
       #elif YI_TVOS || YI_ROKU || YI_SAMSUNGTV || YI_LGTV || YI_DEVICE_SMARTTV
        return CCL_DEVICE_TYPE_SETTOP;
       #elif YI_LINUX || YI_OSX || YI_WINDOWS || YI_WIN32 || YI_WIN64
        return CCL_DEVICE_TYPE_DESKTOP;
       #elif YI_DEVICE_TABLET
        return CCL_DEVICE_TYPE_TABLET;
       #else
        return CCL_DEVICE_TYPE_UNKNOWN;
       #endif
    }
}
