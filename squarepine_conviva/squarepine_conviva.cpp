#include "ConvivaModule.h"

//==============================================================================
#include "source/CompilerWarningDisableStart.h"

//==============================================================================
#ifndef JUCE_CORE_H_INCLUDED

namespace juce
{
    /** Overwrites a structure or object with zeros. */
    template <typename Type>
    inline void zerostruct (Type& structure) noexcept { std::memset ((void*) &structure, 0, sizeof (structure)); }
}

#endif

/** @warning If the destination string is not null, make sure that it has enough
             memory to store the source string.

    Note: Make sure to free the memory after you're done with it
*/
void copyString (char*& destination, const String& source)
{
    if (destination != nullptr)
    {
        strcpy_s (destination, (rsize_t)source.GetLength(), source.GetData());
    }
    else
    {
       #if YI_UWP
        destination = _strdup (source.GetData());
       #else
        destination = strdup (source.GetData());
       #endif
    }
}

template<size_t maxLength>
void copyString (char (&destination)[maxLength], const String& source)
{
    const String subsection (source.SubStr (0, maxLength));
    strcpy_s (destination, (rsize_t)subsection.getSizeInBytes(), subsection.GetData());
}

//==============================================================================
#include "source/MacroCleanups.h"

extern "C"
{
    #include "../library/src/lib/ccl.c"
    #include "../library/src/lib/dictionary.c"
    #include "../library/src/lib/json.c"
    #include "../library/src/lib/list.c"
    #include "../library/src/lib/monitor.c"
    #include "../library/src/lib/protocol.c"
    #include "../library/src/lib/queue.c"
    #include "../library/src/lib/session.c"
    #include "../library/src/lib/settings.c"
    #include "../library/src/lib/util.c"

    #include "../library/src/include/ccl.h"
    #include "../library/src/include/ccl_content_info.h"
    #include "../library/src/include/ccl_dictionary.h"
    #include "../library/src/include/ccl_notifier_if.h"
    #include "../library/src/include/ccl_platform_if.h"
    #include "../library/src/include/ccl_player_if.h"
}

#include "source/MacroCleanups.h"

//==============================================================================
#include "source/RAIIWrappers.h"
#include "source/SystemStats.h"
#include "source/Conviva.cpp"

//==============================================================================
#include "source/CompilerWarningDisableEnd.h"
