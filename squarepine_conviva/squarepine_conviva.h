/** Instead of the old-fashioned integration way, the files have been wrapped
    into a Unity Build module to reduce the integration overhead and compilation overhead.

    Simply bring the ConvivaModule.cpp and ConvivaModule.h files into your project,
    and you can get started with the library!

    You do not need to bring the Conviva files into your project.
    If you do, be sure to NOT compile its definition files (ie: the .c files).

    Note that the Conviva C SDK has been modified to avoid compilation errors.

    For more information on the Unity Build module architectural technique, please refer to:
    https://en.wikipedia.org/wiki/Single_Compilation_Unit
    http://buffered.io/posts/the-magic-of-unity-builds/
*/
#ifndef SQUAREPINE_CONVIVA_MODULE_H
#define SQUAREPINE_CONVIVA_MODULE_H

#include <atomic>
#include <cstring>
#include <map>
#include <memory>
#include <mutex>

//==============================================================================
/** Define this to use locks in the Conviva network queues.

    By default, this is internally enabled.
*/
#ifndef QUEUE_USE_LOCK
    #define QUEUE_USE_LOCK 1
#endif //QUEUE_USE_LOCK

/** Define this to use locks in the Conviva network queues.

    By default, this is internally set to 64.
*/
#ifndef QUEUE_DEFAULT_SIZE
    #define QUEUE_DEFAULT_SIZE 64
#endif

#ifndef CONVIVA_LOG_TYPE
    #define CONVIVA_LOG_TYPE
#endif

/** @see ccl_log_level_t */
#ifndef CONVIVA_PRODUCTION_ERROR_LEVEL
    #define CONVIVA_PRODUCTION_ERROR_LEVEL CCL_LOG_LEVEL_WARN
#endif

#ifndef CONVIVA_SET_USER_AGENT
    //#define CONVIVA_SET_USER_AGENT ""
#endif //CONVIVA_USE_USER_AGENT_HACK

//==============================================================================
#include "source/CompilerWarningDisableStart.h"
#include "source/Conviva.h"
#include "source/CompilerWarningDisableEnd.h"

#endif //YI_CONVIVA_MODULE_H
